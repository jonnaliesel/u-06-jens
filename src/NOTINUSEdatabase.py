from multiprocessing.dummy import connection
import sqlite3


CREATE_JOBS_TABLE = "CREATE TABLE IF NOT EXISTS jobs (id INTEGER PRIMARY KEY, name text);"

INSERT_JOB = "INSERT INTO jobs (name) VALUES(?);"

GET_ALL_JOBS = "SELECT * FROM jobs"
GET_JOBS_BY_NAME = "SELECT * FROM jobs WHERE name = ?;"

#skapa en connection till databsen, vilket blir data.b
def connect():
    return sqlite3.connect("data.db")


#Skapar databasen jobs table
def create_table(connection):
    with connection:
        connection.execute(CREATE_JOBS_TABLE)


# Metod för att addera in jobs från vår request
def add_job(connection, name):
    with connection:
        connection.execute(INSERT_JOB, (name))

# Se alla jobs som finns i jsonObject
def get_all_jobs(connection):
    with connection:
        return connection.execute(GET_ALL_JOBS).fetchall()

# Retunera alla Jobs
def get_jobs_by_name(connection, name):
    with connection:
        return connection.execute(GET_JOBS_BY_NAME, (name,)).fetchall()